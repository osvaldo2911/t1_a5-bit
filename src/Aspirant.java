class Aspirante {

	private String nombre;
	private String PrimerApellido;
	private String SegundoApellido;
	private String edad;
	private String direccion;
	private String telefono;
	private String correoElectronico;
	private String redesSociales;
	private String carreraDeInteres;
	private String escuelaPrecedencia;
	private String bachilleratoCursado;

	public Aspirante(String nombre, String PrimerApellido, String segundoApellido, String edad, String direccion,
			String telefono, String correoElectronico, String redesSociales, String carreraDeInteres,
			String escuelaPrecedencia, String bachilleratoCursado) {
		super();
		this.nombre = nombre;
		PrimerApellido = PrimerApellido;
		SegundoApellido = segundoApellido;
		this.edad = edad;
		this.direccion = direccion;
		this.telefono = telefono;
		this.correoElectronico = correoElectronico;
		this.redesSociales = redesSociales;
		this.carreraDeInteres = carreraDeInteres;
		this.escuelaPrecedencia = escuelaPrecedencia;
		this.bachilleratoCursado = bachilleratoCursado;
	}
	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrimerApeido() {
		return PrimerApellido;
	}

	public void setPrimerApeido(String primerAp) {
		PrimerApellido = primerAp;
	}

	public String getSegundoApellido() {
		return SegundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		SegundoApellido = segundoApellido;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getRedesSociales() {
		return redesSociales;
	}

	public void setRedesSociales(String redesSociales) {
		this.redesSociales = redesSociales;
	}

	public String getCarreraDeInteres() {
		return carreraDeInteres;
	}

	public void setCarreraDeInteres(String carreraDeInteres) {
		this.carreraDeInteres = carreraDeInteres;
	}

	public String getEscuelaPrecedencia() {
		return escuelaPrecedencia;
	}

	public void setEscuelaPrecedencia(String escuelaPrecedencia) {
		this.escuelaPrecedencia = escuelaPrecedencia;
	}

	public String getBachilleratoCursado() {
		return bachilleratoCursado;
	}

	public void setBachilleratoCursado(String bachilleratoCursado) {
		this.bachilleratoCursado = bachilleratoCursado;

	}


	@Override
	public String toString() {
		return "Nombre: " + nombre + "\nPrimer Apellido: " + PrimerApellido + "\nSegundoApellido: " + SegundoApellido
				+ "\nEdad: " + edad + "\nDireccion: " + direccion + "\nTel: " + telefono + "\nCorreo Electronico: "
				+ correoElectronico + "\nRedes Sociales: " + redesSociales + "\nCarrera De Interes: " + carreraDeInteres
				+ "\nEscuela Precedencia: " + escuelaPrecedencia + "\nBachillerato: " + bachilleratoCursado;
	}


	
}
public class Aspirant {

	public static void main(String[] args) {
		

	}

}
