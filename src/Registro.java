class RegistroAspirantes{

	Aspirante[] vectorAspirantes = new Aspirante[100];
	private int posicion = 0;

	public int getPosicion() {
		return posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}

	public void registrarAspirantes(Aspirante aspirante) {
		for (int i = 0; i < vectorAspirantes.length; i++) {
			if(vectorAspirantes[i]==null) {
				vectorAspirantes[i]=aspirante;
				this.setPosicion(getPosicion()+1);
				break;
			}
		}
	}

	public void eliminarAspirante(int pos) {
		vectorAspirantes[pos]=null;
	}

	public void imprimirAspirantes() {
		for (int i = 0; i < vectorAspirantes.length; i++) {
			if(vectorAspirantes[i]!=null) {
				System.out.println("Ficha "+i);
				System.out.println("============================================");
				System.out.println(vectorAspirantes[i]);
				System.out.println("============================================");
				System.out.println();
			}
		}
	}

}
public class Registro {

}
